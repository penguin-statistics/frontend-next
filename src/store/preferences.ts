import { makeAutoObservable } from "mobx";

enum Language {
  ZH = "zh",
  EN = "en",
  JA = "ja",
  KO = "ko",
}

enum Appearance {
  SYSTEM = "system",
  LIGHT = "light",
  DARK = "dark",
}

enum Theme {
  DEFAULT = "default",
  MIKU2021 = "miku2021",
}

export class UserPreferences {
  language: Language;
  appearance: Appearance;
  theme: Theme;

  constructor() {
    this.language = Language.ZH;
    this.appearance = Appearance.SYSTEM;
    this.theme = Theme.DEFAULT;

    makeAutoObservable(this)
  }

  setLanguage(language: Language) {
    this.language = language;
  }

  setAppearance(appearance: Appearance) {
    this.appearance = appearance;
  }

  setTheme(theme: Theme) {
    this.theme = theme;
  }
}
