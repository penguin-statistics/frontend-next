import { UserPreferences } from "./preferences";
import { makeAutoObservable } from 'mobx';

export class RootStore {
  preferences: UserPreferences;
  constructor() {
    this.preferences = new UserPreferences();
    makeAutoObservable(this);
  }
}

export const store = new RootStore();
