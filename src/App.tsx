import { createTheme, CssBaseline, ThemeProvider } from "@mui/material";
import { useState } from "react";
import { Router } from "./utils/router";

function App() {
  const [theme, setTheme] = useState(createTheme());

  return (
    <ThemeProvider theme={theme}>
      <button onClick={() => {
        setTheme(createTheme({
          palette: {
            primary: {
              main: '#ff0000'
            }
          }
        }))
      }}>
        Change Theme
      </button>
      <CssBaseline />
      <Router />
    </ThemeProvider>
  );
}

export default App;
