import { Button, Typography } from "@mui/material";
import { FC } from "react";
import { RootStore } from '../store/root';

const HomePage: FC = () => {
  const store = RootStore

  return (
    <div>
      <h1>Index Page</h1>
      <p>This is the index page</p>
      
    </div>
  );
};

export { HomePage }
