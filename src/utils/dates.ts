import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import chinese from "dayjs/locale/zh-cn";

export type DayjsDate = string | number | dayjs.Dayjs | Date | null | undefined;

dayjs.extend(relativeTime);
dayjs.locale("zh-cn", chinese);

export function formatDate(date: DayjsDate, format?: string) {
  return dayjs(date).format(format);
}

export function formatDateRelative(date: DayjsDate) {
  return dayjs(date).fromNow();
}
