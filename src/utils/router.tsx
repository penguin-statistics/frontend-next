import { BrowserRouter, Route, Routes } from "react-router-dom";
import { AppLayout } from "src/components/layouts/AppLayout";
import { HomePage } from "../pages/home";

export const Router = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<AppLayout />}>
          <Route index element={<HomePage />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}
