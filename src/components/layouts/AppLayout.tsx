import { FC } from "react";
import { Outlet } from "react-router";

const AppLayout: FC = () => {
  return (
    <div>
      <h1>AppLayout</h1>
      <Outlet />
    </div>
  );
};

export { AppLayout };
