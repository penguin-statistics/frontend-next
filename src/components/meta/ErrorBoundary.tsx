import { Component, ComponentType } from "react";

interface ErrorBoundaryProps {
  title?: string;
}

export class ErrorBoundary extends Component<
  ErrorBoundaryProps,
  { hasError: boolean }
> {
  constructor(props: ErrorBoundaryProps) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error: any) {
    return { hasError: true };
  }

  componentDidCatch(error: any, errorInfo: any) {
    console.error(error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>;
    }

    return this.props.children;
  }
}

export function errorBoundary<P>(
  Komponent: ComponentType<P>,
  errorBoundaryProps?: ErrorBoundaryProps
): ComponentType<P> {
  const Wrapped: ComponentType<P> = (props) => {
    return (
      <ErrorBoundary {...errorBoundaryProps}>
        <Komponent {...props} />
      </ErrorBoundary>
    );
  };

  // Format for display in DevTools
  const name = Komponent.displayName || Komponent.name || "Unknown";
  Wrapped.displayName = `withErrorBoundary(${name})`;

  return Wrapped;
}

export const composeErrorBoundary = (...HOCs) =>
  [errorBoundary, ...HOCs].reduce(
    (a, b) =>
      (...args) =>
        a(b(...args)),
    (arg) => arg
  );
