<img src="https://penguin.upyun.galvincdn.com/logos/penguin_stats_logo.png"
     alt="Penguin Statistics - Logo"
     width="96px" />
     
# Penguin Statistics - V3 Frontend
[![License](https://img.shields.io/github/license/penguin-statistics/frontend-next)](https://github.com/penguin-statistics/frontend-next/blob/dev/LICENSE)
[![Last Commit](https://img.shields.io/github/last-commit/penguin-statistics/frontend-next)](https://github.com/penguin-statistics/frontend-next/commits/dev)
[![GitHub Actions Build Status](https://github.com/penguin-statistics/frontend-next/actions/workflows/go.yml/badge.svg)](https://github.com/penguin-statistics/frontend-next/actions/workflows/go.yml)

![Alt](https://repobeats.axiom.co/api/embed/9684d0f897b5d0e1c2fbcf14d6fcc92416e30043.svg "Repobeats analytics image")

The re-designed [Penguin Statistics](https://penguin-stats.io/?utm_source=github) v3 Frontend, built with React.

## Maintainers
This project has mainly being maintained by the following contributors (in alphabetical order):
- [GalvinGao](https://github.com/GalvinGao)

> The full list of active contributors of the *Penguin Statistics* project can be found at the [Team Members page](https://penguin-stats.io/about/members) of the website.